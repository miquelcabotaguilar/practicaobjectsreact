import './App.css';
import axios from "axios";
import {Link} from "react-router-dom";
import {useState, useEffect} from "react";
import NoBuckets from "./components/modals/error/noBuckets";
import BucketItem from "./components/BucketItem";
import Loading from "./components/Loader";
import UploadBucket from "./components/modals/uploads/uploadBuckets";


const App = () => {

    const [userLogged, setUserLogged] = useState(false);
    const [errorModal, setErrorModal] = useState(false);
    const [bucketList, setBucketList] = useState([]);
    const [errorMessage, setErrorMessage] = useState('');
    const [loading, setLoading] = useState(true);
    const [pageVis, setPageVis] = useState(false);

    let key = sessionStorage.getItem("user_name");
    let url = `http://www239.cfgs.esliceu.net/objects?prefix=${key}`;
    let token = sessionStorage.getItem("accessToken");

    useEffect(() => {
        if(token !== null) {
            axios.get(`${url}`, {
                headers: {
                    Authorization: 'Bearer ' + sessionStorage.getItem("accessToken"),
                    'Content-Type': 'application/json'
                }
            })
                .then((res) => {
                    console.log(res);
                    setBucketList(res.data.versions);
                    setPageVis(true);
                    setLoading(false);
                })
                .catch((err) => {
                    setErrorMessage(err.message);
                    setErrorModal(true);
                    setPageVis(true);
                    setLoading(false);
                })
        }
    }, [token, url]);

    useEffect(() => {

        if(token !== null) {
            setUserLogged(true);
            setPageVis(true);
            setLoading(false);
        } else {
            setUserLogged(false);
            setPageVis(true);
            setLoading(false);
        }
    }, [token]);

    if(!pageVis) {
        return (
            <div>
                <Loading />
            </div>
        )
    }

    if(userLogged === true) {
        return(
            <main>
                <div className="container">
                    <div>
                        <NoBuckets showAlert={errorModal} error={errorMessage} />
                    </div>

                        <div className="caja">

                            <div>
                                <h1>Buckets de l'Usuari</h1>
                                <UploadBucket />
                            </div>
                            {
                                loading !== true ?
                                    bucketList.map((bucket, i) => {
                                        return i % 2 === 0 ?
                                            <BucketItem bucket={bucket} key={i} />
                                            :
                                            <BucketItem bucket={bucket} key={i} />;
                                    })
                                :
                                <Loading />
                            }

                        </div>
                </div>
            </main>
        )
    } else {
        return(
            <main>
                <div className="container">
                    <div className="card__container">
                        <div className="card">
                            <div className="card__content">
                                <h1>
                                    Benvingut a Buckets
                                </h1>
                                <br/>
                                <Link to="/login" className="card__button">Inicia sesión</Link>
                                <label> o </label>
                                <Link to="/signup" className="card__button">Registre</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}

export default App;
