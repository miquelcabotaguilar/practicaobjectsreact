import{ Link } from "react-router-dom";
import {useState} from "react";
import axios from "axios";
import Loader from "../components/Loader";

const login = () => {

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [username, setUsername] = useState();
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [password, setPassword] = useState();
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [pageVis,  setPageVis] = useState(false);

    const login = async (e) => {
        e.preventDefault();
        setPageVis(true);
        setTimeout(() => {
            axios.post('http://www239.cfgs.esliceu.net/login', {
                username: username,
                password: password
            })
                .then((res) => {
                    console.log(res);
                    sessionStorage.setItem("accessToken", res.data.accessToken);
                    sessionStorage.setItem("user_name", username);
                    sessionStorage.setItem("old_password", password);
                    window.location = "/";
                })
                .catch((err) => {
                    console.log(err);
                });
        }, 1500)
    }

    if(pageVis){
        return <Loader/>;
    }

    return(
        <>
            <div>
                <main>
                    <div className="container">
                        <div className="card__container">
                            <div className="card">
                                <div className="card__content">
                                    <form onSubmit={e => {login(e)}}>
                                        <h1 className="card__header">Login</h1>
                                        <label className="card__info"> Username: </label>
                                        <input
                                            type="text"
                                            value={username}
                                            onChange={e => setUsername(e.target.value)}
                                            placeholder="user"
                                        />
                                        <br/>
                                        <label className="card__info"> Password: </label>
                                        <input
                                            type="password"
                                            value={password}
                                            onChange={e => setPassword(e.target.value)}
                                            placeholder="password"
                                        />
                                        <br/>
                                        <button type="submit" className="card__button" value="fer login amb l'ususari proporcionat">Log-in</button>
                                        <label> - o - </label>
                                        <Link to="/signup" className="card__button"> Registre </Link>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </>
    );
};

export default login;