import{ Link } from "react-router-dom";
import { useState } from "react";

const register = () => {

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [username, setUsername] = useState();
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [password, setPassword] = useState();

    const validateUsername = (username) => {
        return username !== null;
    }

    const validatePassword = (password) => {
        return password !== null;
    }

    const doRegister = async (e) => {
        e.preventDefault();

        let validUser = validateUsername(username);
        let validPwd = validatePassword(password);

        if(validPwd && validUser) {
            fetch('http://www239.cfgs.esliceu.net/signup', {
                method:'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({username: username ,password:password})
            })
                .then((response) => response.json())
                .then((res) => {
                    localStorage.setItem("id", res.id);
                })
                .catch((err) => {
                    console.log(err);
                });
        }

    }

    return (
        <>
            <div>
                <main>
                    <div className="container">
                        <div className="card__container">
                            <div className="card">
                                <div className="card__content">
                                    <form onSubmit={e =>  {doRegister(e)}}>
                                        <h1 className="card__header">Registre</h1>
                                        <label className="card__info"> Username: </label>
                                        <input type="text" id="username" placeholder="user" value={username} onChange={e => setUsername(e.target.value)}/>
                                        <br/>
                                        <label className="card__info"> Password: </label>
                                        <input type="password" id="password" value={password} onChange={e => setPassword(e.target.value)} placeholder="password"/>
                                        <br/>
                                        <button type="submit" id="register" className="card__button"> Registret </button>
                                        <label> - O -</label>
                                        <Link to="/" className="card__button"> Ja tinc un compte </Link>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </>
    );

};

export default register;