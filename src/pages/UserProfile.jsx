import {useState, useEffect} from "react";
import Loader from "../components/Loader";
import PasswordAccountForm from "../components/user_profile/PasswordForm";
import DeleteAccountForm from "../components/user_profile/DeleteAccountForm";
import axios from "axios";

const UserProfile = () => {

    let user = {
        name: null,
        created_at: null,
        id: null,
    }
    
    // eslint-disable-next-line no-unused-vars
    let error = '';
    
    const [userData, setUserData] = useState(user);
    const [pageVis, setPageVis] = useState(false);
    const [errorMessage, seterrorMessage] = useState(false);
    
    let url = 'http://www239.cfgs.esliceu.net/user';
    
    useEffect(() => {
    
        setTimeout(() => {
            axios.get(`${url}`, {
                headers: {
                    Authorization: 'Bearer ' + sessionStorage.getItem("accessToken")
                }
            })
            .then((res) => {
    
                user.name = res.data.username;
                user.created_at = res.data.created_at;
                user.id = res.data.id;
    
                setUserData(user);
    
                setPageVis(true);
            })
            .catch(() => {
                seterrorMessage(true);
                setPageVis(true);
                // eslint-disable-next-line react-hooks/exhaustive-deps
                error = 'Inicia sessio per favor';
                sessionStorage.removeItem("accessToken");
                setTimeout(() => {
                    window.location.reload();
                }, 3000)
            })
        }, 1000)
    
    }, []);
    
    if(user.id === null && sessionStorage.getItem("accessToken") === null) {
        return window.location = '/login'
    }
    
    if(pageVis && errorMessage) {
        return (
            <div>
                En cas de que el teu navegador no recarregi automaticament usa aquest <a href="/login">Enllaç</a>
            </div>
        )
    }
    
    if(!pageVis) {
        return (
            <div>
                <Loader />
            </div>
        )

    } else {
        return (
            <>
                <div className="container">
                    <div className="caja">
                        <h1>Benvingut! {userData.name} </h1>
                        <PasswordAccountForm />
                        <DeleteAccountForm />
                    </div>
                </div>
            </>
        )
    }

}

export default UserProfile;