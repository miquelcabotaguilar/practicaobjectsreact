import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from "./App";
import Header from './components/Header';
import Login from "./pages/Login";
import Register from "./pages/Register";
import UserProfile from "./pages/UserProfile";
import SingleBucket from "./components/SingleBucket";
import reportWebVitals from './reportWebVitals';
import Footer from "./components/footer";


import { BrowserRouter, Routes, Route } from "react-router-dom";

let item = localStorage.getItem

ReactDOM.render(
  <React.StrictMode>
      <Header />
      <BrowserRouter>
      <Routes>
          <Route path="/" element={<App/>}/>
          {/*Pagines de els Buckets*/}
          <Route path={`/objects/${localStorage.getItem("user_name")}/:key`} element={<SingleBucket />} />
          {/*Pagines del usuari*/}
          <Route path="/user_profile" element={<UserProfile />} />
          {/*Formularis de registre i inici*/}
          <Route path="/login" element={<Login />} />
          <Route path="/signup" element={<Register />} />
      </Routes>
      </BrowserRouter>
      <Footer  />
  </React.StrictMode>,
  document.getElementById('root')
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
