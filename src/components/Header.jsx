import LoggedHeader from "./Headers/LoggedHeader";
import NoLoggedHeader from "./Headers/NoLoggedHeader";

const Header = () => {
    let token = sessionStorage.getItem("accessToken");
    if(token !== null){
        return <LoggedHeader/>
    }else{
        return <NoLoggedHeader/>
    }
}

export default Header;