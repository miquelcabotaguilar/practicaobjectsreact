
const LoggedHeader = () => {

    const logout = (e) => {
        e.preventDefault();
        sessionStorage.removeItem('accessToken');
        window.location = '/';
    }

    return(
        <>
            <header>
                <nav>
                    <div>
                        <nav>
                            <ul className="menu-bar">
                                <li className="opcio-menu">
                                    <a href="/" data-item="Buckets">Buckets</a>
                                </li>
                                <li className="opcio-menu">
                                    <a href="/user_profile/">Perfil d'usuari</a>
                                </li>
                                <li className="opcio-menu">
                                    <a href="/" onClick={e =>{logout(e)}}>Fer Logout!</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </nav>
            </header>
        </>
    );

}

export default LoggedHeader;