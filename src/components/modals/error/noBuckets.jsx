import { useState, useEffect } from "react";

const NoBuckets = (props) => {
    const [showAlert, setShowAlert] = useState(false);

    useEffect(() => {
        setShowAlert(props.showAlert)
    }, []);

    return (
        <>
            {showAlert ? (
                <div>
            <span>
              <i/>
            </span>
                    <span>
              <b>Error!</b> {props.error}
            </span>
                    <button onClick={() => setShowAlert(false)}>
                        <span>×</span>
                    </button>
                </div>
            ) : null}
        </>
    );
}

export default NoBuckets;