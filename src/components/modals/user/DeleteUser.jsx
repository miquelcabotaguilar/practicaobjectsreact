import axios from "axios";
import { useState } from "react";

const DeleteUser = () => {
    const [showModal, setShowModal] = useState(false);


    const submitForm = (e) => {
        e.preventDefault();

        axios.delete(`http://www239.cfgs.esliceu.net/user`, {
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem("accessToken"),
                'Content-Type': 'application/json'
            }
        })
            .then(() => {
                localStorage.removeItem("accessToken");
                window.location = '/'
            })
            .catch((err) => {
                alert(err);
            })

        console.log(e);
    }


    return (
        <>
            <div className="distancia">
                <h2>Eliminar Compte?</h2>
                <button id="uploadNewBucket" onClick={() => setShowModal(true)} className="card__button2">
                    Eliminar compte
                </button>
                {showModal ? (
                    <div  className="modal">
                        {/*content*/}
                        <div>
                            {/*header*/}
                            <div className="modal_header">
                                <h3>
                                    Segur que vols eliminar el teu compte?
                                </h3>
                                <button onClick={() => setShowModal(false)} className="close">
                                    <span>
                                        x
                                    </span>
                                </button>
                            </div>

                            <div className="modal_body">
                                <form onSubmit={e => {submitForm(e)}}>
                                    <p>
                                        Recorda que si elimines el teu compte es perdran totes les dades que hagis pujat!
                                    </p>
                                    <div>
                                        <button type="button" onClick={() => setShowModal(false)} className="card__button2">
                                            No, he pensat millor
                                        </button>
                                        <button type="submit" className="card__button2">
                                            Sí, elimina la meva compte
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                ) : null}
            </div>
        </>
    );
}

export default DeleteUser;