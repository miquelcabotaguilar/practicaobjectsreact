import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const UploadBucket = ({visibility}) => {
    const [showModal, setShowModal] = useState(false);
    const [file, setFile] = useState();


    const fileHandler = (e) => {
        setFile(e.target.files[0]);
        console.log(e.target.files[0]);
    }
    const submitForm = (e) => {
        e.preventDefault();

        const data = new FormData();
        data.append('content', file);

        console.log(file.name);
        let userLogged = sessionStorage.getItem('user_name');
        let key = `${userLogged}/${file.name.replaceAll(" ", "_")}`;
        let token = sessionStorage.getItem("accessToken");

        axios.put(`http://www239.cfgs.esliceu.net/objects/${key}`, file, {
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem("accessToken"),
                'Content-Type': 'application/octet-stream',
            }
        })
            .then((res) => {
                setShowModal(false);
                window.location = '/'
            })
            .catch((err) => {
                setShowModal(false);
            })

        console.log(e);
    }

    return (
        <>
            <button id="uploadNewBucket" onClick={() => setShowModal(true)}  className="carta">
                Nou Bucket
            </button>
            {showModal ? (
                <>
                    <div className="modal">
                        <div>
                            <div  className="modal_header">
                                <h3>Pujar un nou archiu</h3>
                                <button onClick={() => setShowModal(false)} className="close">
                                      <span>
                                        ×
                                      </span>
                                </button>
                            </div>

                            <div className="modal_body">
                                <form onSubmit={e => {
                                    submitForm(e)
                                }} encType="multipart/form-data">
                                    <label>
                                        <input type="file" onChange={fileHandler} className="card__button"/>
                                    </label>
                                    <div>
                                        <button type="button" onClick={() => setShowModal(false)}
                                                className="card__button">
                                            Cerrar
                                        </button>
                                        <button type="submit" className="card__button">
                                            Subir bucket
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}

export default UploadBucket;