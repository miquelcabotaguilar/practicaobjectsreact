const Loader = () => {
    return (
        <main>
            <div  className="container">
                <div className="card__container">
                    <div className="card">
                        <div>
                            <h1>Carregant...</h1>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    )
}

export default Loader;