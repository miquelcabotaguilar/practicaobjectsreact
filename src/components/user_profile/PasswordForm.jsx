import axios from "axios";
import {useState} from "react";
import Loader from "../Loader";

const PasswordForm = () => {

    const [pageVis, setPageVis] = useState(true);
    const [password, setPassword] = useState();
    const [password2, setPassword2] = useState();

    const changePassword = (e) => {
        e.preventDefault();

        setPageVis(false);

        if(password !== sessionStorage.getItem("old_password")) {
            if(password === password2 && password.length >= 8) {
                axios.post('http://www239.cfgs.esliceu.net/user/password', {
                        newPassword: password,
                        oldPassword: sessionStorage.getItem("old_password"),
                    },
                    {
                        headers: {
                            Authorization: 'Bearer ' + sessionStorage.getItem("accessToken"),
                        }
                    }
                )
                    .then((res) => {
                        sessionStorage.removeItem("accessToken");
                        sessionStorage.removeItem("user_name");
                        setPageVis(true);
                    })
                    .then(() => {
                        alert("se va a cerrar la sesión para que entres con tus nuevas credenciales");
                        setPageVis(true);
                        window.location = '/login';
                    })
                    .catch((err) => {
                        setPageVis(true);
                        console.log(err.errors[0].message);
                    })
            } else {
                setPageVis(true);
            }
        } else {
            setPageVis(true);
        }

    }

    if(!pageVis) {
        return <Loader />;
    }


    return (

        <div>
            <div className="distancia">
                <div>
                    <h2>Cambiar contraseña</h2>
                </div>
                <div>

                    <form onSubmit={e => {changePassword(e)}}>
                        <div>
                            <p>Nova Password</p>
                            <input
                                type="password"
                                placeholder="password"
                                value={password}
                                id="pwd"
                                onChange={e => setPassword(e.target.value)}
                                style={{ transition: "all .15s ease" }}
                            />
                        </div>

                        <div>
                            <p>Repiteix Password</p>
                            <input type="password" placeholder="password" value={password2} id="password"
                                   onChange={e => setPassword2(e.target.value)} style={{ transition: "all .15s ease" }}
                            />
                        </div>

                        <div>
                            <button type="submit" className="card__button2">
                                Cambiar contraseña
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default PasswordForm;