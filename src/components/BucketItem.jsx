import axios from "axios";
import { useState } from "react";
import Moment from "react-moment";

const BucketItem = ({bucket, bg_color}) => {

    const [display, setDisplay] = useState('');
    const [showModal, setShowModal] = useState(false);


    const downloadFile = (e) => {

        document.getElementsByTagName("body")[0].classList.add("cursor-wait")

        e.preventDefault();
        fetch(`http://www239.cfgs.esliceu.net/objects/${bucket.key}`, {
            method: 'GET',
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem("accessToken")
            },
        })
            .then((response) => response.blob())
            .then((blob) => {

                const url = window.URL.createObjectURL(
                    new Blob([blob]),
                );
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute(
                    'download',
                    `${bucket.key}`,
                );

                document.body.appendChild(link);
                link.click();
                link.parentNode.removeChild(link);

                document.getElementsByTagName("body")[0].classList.remove("cursor-wait")
            })
            .catch((err) => {
                alert(err);
                document.getElementsByTagName("body")[0].classList.remove("cursor-wait")
            });
    }

    const formatBytes = (bytes, decimals = 2) => {
        if (bytes === 0) return '0 Bytes';

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    const deleteItem = (key, versionId) => {
        let url = `http://www239.cfgs.esliceu.net/objects/${bucket.key}`;

        if(versionId !== null) {
            url = `http://www239.cfgs.esliceu.net/objects/${key}?versionId=${versionId}`
        }

        axios.delete(url, {
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem("accessToken"),
                'Content-Type': 'application/json'
            }
        }).then((res) => {
                setDisplay('hidden');
        }).catch((err) => {
                console.log(err);
        });


    }

    return (
    <div className={display}>
        <>
            <button id="uploadNewBucket" onClick={() => setShowModal(true)} className="carta">
                <h2>{bucket.key.split("/")[1]}</h2>
            </button>
            {showModal ? (
                <>
                    <div className="modal">
                        <div>
                            <div  className="modal_header">
                                <h3>{bucket.key.split("/")[1]}</h3>
                                <button onClick={() => setShowModal(false)} className="close">
                                    <span>
                                        ×
                                    </span>
                                </button>

                            </div>
                            <div className="modal_body">
                                <div>
                                    <p>Etag: {bucket.etag.replaceAll('"', "")}</p>
                                    <p>Modificado por última vez: <Moment date={bucket.lastModified} format="DD/MM/YYYY HH:mm:ss" /></p>
                                    <p>Tamaño: {formatBytes(bucket.size)}</p>
                                    <p>Versión actual: {bucket.versionId}</p>
                                </div>

                                <div>
                                    <button onClick={e => downloadFile(e)} className="card__button">Descarregar</button>
                                    <button onClick={() => deleteItem(bucket.key, bucket.versionId)} className="card__button2">Eliminar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    </div>
    )
}
export default BucketItem;